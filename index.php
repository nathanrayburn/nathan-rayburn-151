
<?php


session_start();
require "controler/controler.php";

if(isset($_GET["action"]))
{
    switch($_GET["action"])
    {
        case "home":
            home();
            break;
        case "login":
            login($_POST);
            break;
        case "logout":
            logout();
            break;
        case "snowboards":
            snowboards();
            break;
        case "snowboardsClient":
            snowboardsClient();
            break;
        case "register":
            register($_POST);
            break;

        default:
    }
}
else
{
    home();
}
