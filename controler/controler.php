<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : 151_2019_ForStudents
 * Created  : 05.02.2019 - 18:40
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */

function login($loginRequest)
{
    if(isset($loginRequest["inputUserEmailAddress"]) && isset($loginRequest["inputUserPassword"]))
    {
        $userEmail = $loginRequest["inputUserEmailAddress"];


        $userPassword = hash("sha256", $loginRequest["inputUserPassword"]);



        require "model/userManagement.php";
        if (isLoginCorrect($userEmail, $userPassword))
        {
            unset($_SESSION["registerError"]);
            $_SESSION["userEmail"] = $userEmail;
            $_GET["action"] = "home";
            require "view/home.php";
        }
        else
        {
            $_SESSION["registerError"] = 1;
            $_GET["action"] = "login";
            require "view/login.php";
        }

    }
    else
    {
        $_GET["action"] = "login";
        require "view/login.php";
    }
}

function register($arrayofinputs){

    if(isset($arrayofinputs["registerUserEmailAddress"]) && isset($arrayofinputs["registerUserPassword"]) && isset($arrayofinputs["registerRepeatUserPassword"]))
    {
        $userEmail = $arrayofinputs["registerUserEmailAddress"];
        $userPassword = $arrayofinputs["registerUserPassword"];
        $userRepeatPassword = $arrayofinputs["registerRepeatUserPassword"];
        if($userPassword == $userRepeatPassword){
            unset($_SESSION["registerError"]);
            require "model/userManagement.php";

            if (isRegisterCorrect($userEmail, $userPassword))
            {
                unset($_SESSION["registerError"]);
                $_SESSION["userEmail"] = $userEmail;
                $_GET["action"] = "home";
                require "view/home.php";
            }
            else
            {
                $_SESSION["registerError"] = 1;
                $_GET["action"] = "register";
                require "view/register.php";
            }
        }else{

            $_GET["action"] = "register";
            require "view/register.php";
        }



    }
    else
    {
        $_SESSION["registerError"]=1;
        $_GET["action"] = "register";
        require "view/register.php";
    }
    return $arrayofinputs;
}
function createSession($userEmail){
    $_SESSION["userEmail"] = $userEmail;

}
function logout()
{
    $_SESSION = array();
    session_destroy();
    $_GET["action"] = "home";
    require "view/home.php";
}
//Home
function home()
{
    require "view/home.php";
}
//Snow Boards
function snowboards(){
    require "model/userManagement.php";
    $snowsResults = selectSnowboards();
    require "view/snowboards.php";
    return $snowsResults;
}
function snowboardsClient(){
    require "model/userManagement.php";
    $snowsResults = selectSnowboards();
    require "view/snowClient.php";
    return $snowsResults;

}

