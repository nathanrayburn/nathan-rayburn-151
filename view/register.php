<?php
/**
 * Created by PhpStorm.
 * User: Jerome.JAQUEMET
 * Date: 08.02.2019
 * Time: 13:56
 */

ob_start();
$titre="RentASnow - Register";

?>


<article>

    <form class="form" method="POST" action="index.php?action=register">
        <div class="container">
            <!--Add Error when register is false


            -->
            <h1>S'inscrire</h1>
            <label for="userEmail"><b>Username</b></label>
            <input type="email" placeholder="Place email address" name="registerUserEmailAddress" required>

            <label for="userPsw"><b>Password</b></label>
            <input type="password" placeholder="Enter password" name="registerUserPassword" required>
            <label for="userPsw"><b>Repeat Password</b></label>
            <input type="password" placeholder="Repeat password" name="registerRepeatUserPassword" required>
        </div>
        <?php if(isset($_SESSION["registerError"])){
            echo "<p class='text-error'>Veuillez resaisir vos données *</p>";
        }
        ?>
        <div class="container">
            <button type="sumbit" class="btn btn-default">Register</button>

        </div>
    </form>
</article>

<?php
$contenu = ob_get_clean();
require "gabarit.php";


?>



