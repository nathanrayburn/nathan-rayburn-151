<?php
/**
 * Created by PhpStorm.
 * User: Jerome.JAQUEMET
 * Date: 08.02.2019
 * Time: 13:56
 */

ob_start();
$titre="RentASnow - Snowboards";

?>

    <table class="table">
    <thead class="table">
    <tr>
        <h1 >Nos Snows</h1>
    </tr>
    <tr>
            <th scope="col">Code</th>
            <th scope="col">Marque</th>
            <th scope="col">Modele</th>
            <th scope="col">Longueur</th>
            <th scope="col">Prix</th>
            <th scope="col">Disponibilite</th>
            <th scope="col">Photo</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $snows = $snowsResults;
    foreach ($snows as $result) : ?>

        <tr>
            <td scope="row"><a href="index.php?action=displayASnow&code=<?= $result['code']; ?>"><?= $result['code']; ?></a></td>
            <td scope="row"><?= $result['brand']; ?></td>
            <td scope="row"><?= $result['model']; ?></td>
            <td scope="row">Longueur : </strong><?= $result['snowLength']; ?> cm</td>
            <td scope="row"> CHF <?= $result['dailyPrice']; ?>.- / jour</td>
            <td scope="row"><?= $result['qtyAvailable']; ?></td>
            <td scope="row"><img class="thumbnail" style ="max-height:50px;max-width:50px;" src="<?= $result['photo']; ?>" alt="<?= $result['code']; ?>" ></td>

            <td scopte ="row">
                <a href="" class="btn btn-small">Modify</a> <a class="btn btn-warning">Delete</a>
            </td>
        </tr>

    <?php endforeach ?>




    </tbody>
    <tr>

    </tr>
    <?php
        /* Affichage annonces dynamique
         * foreach($)
         */
        ?>
</table>
<?php
$contenu = ob_get_clean();
require "gabarit.php";


?>