<?php
/**
 * Created by PhpStorm.
 * User: Jerome.JAQUEMET
 * Date: 13.02.2019
 * Time: 14:05
 */
/**
 * @param $userEmailAddress
 * @param $userPassword
 * @return bool
 */
function isLoginCorrect($userEmailAddress, $userPassword)
{
    $isLoginCorrect = false;

    $strSep = '\'';

   $loginQuery = "SELECT userType,pseudo, userPsw FROM users WHERE userEmailAddress = ".$strSep.$userEmailAddress.$strSep;

    require "model/dbConnector.php";

    $queryResult = executeQuery($loginQuery);

    if(count($queryResult) == 1)
    {
        $userHashedPassword = $queryResult[0]["userPsw"];
        if($userHashedPassword == $userPassword){
            $_SESSION["userType"] = $queryResult[0]["userType"];
            $isLoginCorrect = true;
        }

    }

    return $isLoginCorrect;
}

/**
 * @param $userEmailAddress
 * @param $userPassword
 * @return bool
 */
function isRegisterCorrect($userEmailAddress,$userPassword){
    $registerCorrect = false;
    $strSep = '\'';
    $query = $uniqueQuery = "SELECT pseudo, userPsw FROM users WHERE userEmailAddress = ".$strSep.$userEmailAddress.$strSep;
    require_once "model/dbConnector.php";
    $queryResult = executeQuery($query);
    if(count($queryResult) == 0)
    {
        $userHashedPassword = hash("sha256", $userPassword);
        $insertionQuery="INSERT INTO users(userEmailAddress,userPsw) VALUES('".$userEmailAddress."','".$userHashedPassword."');";
        executeQuery($insertionQuery);
        $registerCorrect=true;
    }

    return $registerCorrect;
}

/**
 * @return array|null
 */
function selectSnowboards()
{

    $snowQuery = "SELECT code,brand,model,snowLength,qtyAvailable,description,dailyPrice,photo,active FROM snows";

    require "model/dbConnector.php";

    $queryResult = executeQuery($snowQuery);


    return $queryResult;

}